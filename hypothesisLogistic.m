function h = hypothesisLogistic(theta,X)
  h = 1./(1+exp(-X*theta));
