function cost = costFunctionLogistic(theta,X,y)
  cost = -y.*log(hypothesisLogistic(theta,X))-(1-y).*log(1-hypothesisLogistic(theta,X));
