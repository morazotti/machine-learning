function Theta = gradientDescentLogistic(alpha,X,y)
  theta=zeros(size(X,2),1);
  for i =1:300,
    theta=theta-alpha*X'*(hypothesisLogistic(theta,X)-y)/size(X,1);
  end
  Theta=theta;
